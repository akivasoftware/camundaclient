﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CamundaClient.Worker
{
    public class MaxRetriesErrorException : Exception
    {
        public string BusinessErrorCode { get; set; }

        public MaxRetriesErrorException(string businessErrorCode, string message)
        : base(message)
        {
            BusinessErrorCode = businessErrorCode;
        }

    }
}
