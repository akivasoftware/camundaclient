﻿using System;
using System.Collections.Generic;
using System.Net;

namespace CamundaClient.Worker
{
    [System.AttributeUsage(System.AttributeTargets.All)
    ]
    public sealed class ExternalTaskTopicAttribute : System.Attribute
    {
        public string TopicName { get; set; }
        public string[] TopicsName { get; set; }
        public int Retries { get; set; } = 5; // default: 5 times
        public long RetryTimeout { get; set; } = 10 * 1000; // default: 10 seconds
        public bool DisableIncidents { get; set; } = false;
        public string WorkerId { get; set; } = $"{Environment.UserName}@{Environment.MachineName}-{Guid.NewGuid()}";
        public long PollingIntervalInMilliseconds { get; set; } = 1000; // default: 1 second
        public int MaxDegreeOfParallelism { get; set; } = 1;
        public int MaxTasksToFetchAtOnce { get; set; } = 1;
        public long LockDurationInMilliseconds { get; set; } = 10 * 60 * 1000; // default: 1 minute
        public bool UsePriority { get; set; } = false;

        //[Obsolete("Use properties params instead.")]
        //public ExternalTaskTopicAttribute(string topicName)
        //{
        //    TopicName = topicName;
        //}

        //[Obsolete("Use properties params instead.")]
        //public ExternalTaskTopicAttribute(string topicName, int retries, long retryTimeout)
        //{
        //    TopicName = topicName;
        //    Retries = retries;
        //    RetryTimeout = retryTimeout;
        //}
        //[Obsolete("Use properties params instead.")]
        //public ExternalTaskTopicAttribute(string topicName, int retries, long retryTimeout, bool disableIncidentes)
        //{
        //    TopicName = topicName;
        //    Retries = retries;
        //    RetryTimeout = retryTimeout;
        //    DisableIncidents = disableIncidentes;
        //}

        public ExternalTaskTopicAttribute() { }
    }
}
