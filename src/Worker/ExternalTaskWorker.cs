﻿using CamundaClient.Dto;
using CamundaClient.Service;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CamundaClient.Worker
{
    public class ExternalTaskWorker : IDisposable
    {
        private string workerId => taskWorkerInfo.WorkerId;

        private Timer taskQueryTimer;
        private long PollingIntervalInMilliseconds => taskWorkerInfo.PollingIntervalInMilliseconds;
        private int MaxDegreeOfParallelism => taskWorkerInfo.MaxDegreeOfParallelism;
        private int MaxTasksToFetchAtOnce => taskWorkerInfo.MaxTasksToFetchAtOnce;
        private long LockDurationInMilliseconds => taskWorkerInfo.LockDurationInMilliseconds;
        private bool UsePriority => taskWorkerInfo.UsePriority;
        private readonly ExternalTaskService externalTaskService;
        private readonly ExternalTaskWorkerInfo taskWorkerInfo;

        public ExternalTaskWorker(ExternalTaskService externalTaskService, ExternalTaskWorkerInfo taskWorkerInfo)
        {
            this.externalTaskService = externalTaskService;
            this.taskWorkerInfo = taskWorkerInfo;
        }

        public void DoPolling()
        {
            // Query External Tasks
            try
            {
                var tasks = externalTaskService.FetchAndLockTasks(workerId, MaxTasksToFetchAtOnce, taskWorkerInfo.TopicsName ?? new List<string> { taskWorkerInfo.TopicName }, LockDurationInMilliseconds, taskWorkerInfo.VariablesToFetch, UsePriority);

                // run them in parallel with a max degree of parallelism
                Parallel.ForEach(
                    tasks,
                    new ParallelOptions { MaxDegreeOfParallelism = this.MaxDegreeOfParallelism },
                    externalTask => Execute(externalTask)
                );
            }
            catch (Exception ex)
            {
                // Most probably server is not running or request is invalid
                Console.WriteLine(ex.Message);
            }

            // schedule next run (if not stopped in between)
            if (taskQueryTimer != null)
            {
                taskQueryTimer.Change(TimeSpan.FromMilliseconds(PollingIntervalInMilliseconds), TimeSpan.FromMilliseconds(Timeout.Infinite));
            }
        }

        private void Execute(ExternalTask externalTask)
        {
            Dictionary<string, object> resultVariables = new Dictionary<string, object>();

            Console.WriteLine($"Execute External Task from topic '{taskWorkerInfo.TopicName}': {externalTask}...");
            try
            {
                taskWorkerInfo.TaskAdapter.Execute(externalTask, ref resultVariables);
                Console.WriteLine($"...finished External Task {externalTask.Id}");

                try
                {
                    externalTaskService.Complete(workerId, externalTask.Id, resultVariables);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($" Exception first complete: {ex.ToString()}");
                    Thread.Sleep(Convert.ToInt32(PollingIntervalInMilliseconds));
                    externalTaskService.Complete(workerId, externalTask.Id, resultVariables);
                }
            }
            catch (UnrecoverableBusinessErrorException ex)
            {
                Console.WriteLine($"...failed with business error code from External Task  {externalTask.Id}");
                externalTaskService.Error(workerId, externalTask.Id, ex.BusinessErrorCode);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"...failed External Task  {externalTask.Id}");
                Console.WriteLine($" Exception: {ex.ToString()}");
                var retriesLeft = taskWorkerInfo.Retries; // start with default

                if (externalTask.Retries.HasValue) // or decrement if retries are already set
                    retriesLeft = externalTask.Retries.Value - 1;

                if (taskWorkerInfo.DisableIncidents && retriesLeft == 0)
                {
                    Console.WriteLine($"...max retries reached and active for finish External Task {ex.Message}");
                    externalTaskService.Error(workerId, externalTask.Id, String.Concat("Max retries reached and active for finish External Task, message: ", ex.Message));
                }
                else
                {
                    externalTaskService.Failure(workerId, externalTask.Id, ex.Message, retriesLeft, taskWorkerInfo.RetryTimeout);
                }
            }
        }

        public void StartWork()
        {
            this.taskQueryTimer = new Timer(_ => DoPolling(), null, PollingIntervalInMilliseconds, Timeout.Infinite);
        }

        public void StopWork()
        {
            this.taskQueryTimer.Dispose();
            this.taskQueryTimer = null;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.taskQueryTimer.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
