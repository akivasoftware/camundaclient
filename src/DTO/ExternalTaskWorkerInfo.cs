﻿using System;
using System.Collections.Generic;
using CamundaClient.Worker;

namespace CamundaClient.Dto
{
    public class ExternalTaskWorkerInfo
    {

        public string WorkerId { get; set; }
        public int Retries { get; set; }
        public long RetryTimeout { get; set; }
        public Type Type { get; set; }
        public string TopicName { get; set; }
        public List<string> TopicsName { get; set; }
        public List<string> VariablesToFetch { get; set; }
        public IExternalTaskAdapter TaskAdapter { get; set; }
        public bool DisableIncidents { get; set; }
        public long PollingIntervalInMilliseconds { get; set; }
        public int MaxDegreeOfParallelism { get; set; }
        public int MaxTasksToFetchAtOnce { get; set; }
        public long LockDurationInMilliseconds { get; set; }
        public bool UsePriority { get; set; }
    }
}
