﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CamundaClient.Dto;

namespace CamundaClient.Requests
{
    class CompleteRequest
    {
        public string businessKey { get; set; }
        public Dictionary<string, Variable> variables { get; set; }
        public string workerId { get; set; }
    }
}
